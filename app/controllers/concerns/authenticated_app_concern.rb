module AuthenticatedAppConcern
  extend ActiveSupport::Concern

  included do
    protect_from_forgery with: :exception

    before_action :require_authentication
    before_action :require_authorization
    before_action :set_cache_control_header

    helper AuthenticatedApp::Engine.helpers

    attr_accessor :current_user
    helper_method :current_user

    helper_method :app_root_url
    helper_method :app_home_url

    rescue_from ActionController::InvalidAuthenticityToken, with: :render_session_problem
    rescue_from UnauthenticatedRequest,                     with: :render_unauthenticated
    rescue_from UnauthorizedRequest,                        with: :render_unauthorized
    rescue_from SuspendedRequest,                           with: :render_suspended
    rescue_from ForbiddenRequest,                           with: :render_unauthorized
  end


  class_methods do

  end

  protected

  def alert_layout
    'application'
  end

  def app_root_url
    self.main_app.send(AuthenticatedApp.root_url || 'root_url')
  end

  def app_home_url
    self.main_app.send(AuthenticatedApp.home_url || 'home_url')
  end

  # to be overridden
  def is_authorized!
  end

  # to be overridden
  def may_authenticate!(user)
  end

  def require_authentication
    @authentication_required = true
    if session[:user_id]
      user = User.find_by(id: session[:user_id])
    end
    if !user
      raise UnauthenticatedRequest
    else
      may_authenticate!(user)
      self.current_user = user
    end
  end

  def optional_authentication
    if session[:user_id]
      user = User.find_by(id: session[:user_id])
      if user
        may_authenticate!(user)
        self.current_user = user
      end
    end
  end

  def require_authorization
    return unless @authentication_required # it does not make sense to require
                                           # authorization if authentication is not required.
    if !current_user
      raise UnauthorizedRequest
    end
    is_authorized!
  end

  def set_cache_control_header
    # don't use 'no-cache'. only 'no-store' will prevent the browser from caching pages.
    response.headers["Cache-Control"] = "max-age=0, private, no-store"
  end

  def render_session_problem
    if session[:cookies_are_enabled] != true
      flash.now[:danger] = t(:cookies_not_enabled)
    else
      flash.now[:danger] = t(:session_expired)
    end
    #reset_session
    render 'authenticated_app/sessions/new'
  end

  def render_unauthenticated
    if request.method == "GET"
      session[:redirect_path] = request.fullpath
    end
    redirect_to authenticated_app.new_session_url
  end

  def render_unauthorized
    render layout: alert_layout, template: "authenticated_app/errors/unauthorized", status: 403
  end

  def render_suspended
    render layout: alert_layout, template: "authenticated_app/errors/suspended"
  end

  def permission_denied!
    raise UnauthorizedRequest
  end
end