class AuthenticatedApp::SessionsController < ApplicationController
  protect_from_forgery with: :exception
  skip_before_action :require_authentication, :only => [:new, :create]
  skip_before_action :require_authorization

  def new
  end

  def create
    user = find_user
    if user.nil?
      handle_no_user
    else
      may_authenticate!(user)
      redirect_path = session[:redirect_path] || app_home_url
      last_login_on = user.last_login_on
      if log_in(user)
        #if last_login_on
        #  flash[:info] = "You last logged in on " + last_login_on.to_date.to_s(:long_ordinal)
        #end
        redirect_to redirect_path
      else
        render_incorrect
      end
    end
  rescue SuspendedRequest
    handle_authenticate_unavailable(user)
  rescue UnauthorizedRequest, ForbiddenRequest
    handle_authenticate_forbidden(user)
  end

  def destroy
    if current_user
      reset_session
      flash[:info] = "You have been logged out"
    end
    redirect_to app_root_url
  end

  private

  def find_user
    login = (params[:login]||'').downcase.strip
    user  = User.find_by_login(login)
  end

  def handle_no_user
    log_auth_failure
    render_incorrect
  end

  def handle_authenticate_unavailable(user)
    log_auth_failure(user)
    render_unavailable
  end

  def handle_authenticate_forbidden(user)
    log_auth_failure(user)
    render_forbidden
  end

  def log_auth_failure(user = nil)
     #params[:username]
     #request.remote_ip
  end

  def render_incorrect
    flash.now[:danger] = I18n.t(:username_or_password_is_incorrect)
    render :new
  end

  def render_unavailable
    flash.now[:danger] = I18n.t(:user_maintenance)
    render :new
  end

  def render_forbidden
    flash.now[:danger] = I18n.t(:user_forbidden)
    render :new
  end

  def log_in(user)
    if user.authenticated?(params[:password])
      self.current_user = user
      reset_session # important to prevent session fixation!
      session[:user_id]   = user.id
      session[:domain_id] = user.domain_object.id if user.respond_to?(:domain_object)
      return true
    else
      return false
    end
  end
end
