
module AuthenticatedApp
  module ApplicationHelper
    def logout_button
      render 'authenticated_app/sessions/logout_button'
    end

    def send_password_reset_email(user, email=nil, domain=nil)
      email ||= user.email
      if user.create_recovery_token(email)
        if domain
          protocol = Rails.env.production? ? 'https://' : 'http://'
          url = protocol + domain + authenticated_app.reset_password_link_path(recovery_user: url_encode_email(user.login), token: user.recovery_token)
        else
          url = authenticated_app.reset_password_link_url(recovery_user: url_encode_email(user.login), token: user.recovery_token)
        end
        AuthenticatedApp::Mailer.reset_password(
          user: user,
          recipient: email,
          link: url
        ).deliver_later
        return true
      else
        return false
      end
    end

    def url_encode_email(email)
      email&.gsub('@', '+at+')&.gsub('.','+dot+')
    end

    def url_decode_email(email)
      email&.gsub(/[ +]at[ +]/, '@')&.gsub(/[ +]dot[ +]/, '.')
    end
  end
end
