$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "authenticated_app/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "authenticated_app"
  spec.version     = AuthenticatedApp::VERSION
  spec.authors     = ["elijah"]
  spec.email       = ["elijah@riseup.net"]
  spec.homepage    = "https://0xacab.org/calyx/authenticated_app"
  spec.summary     = "A mountable Rails Engine providing basic authentication and session management."
  spec.description = "A mountable Rails Engine providing basic authentication and session management."
  spec.license     = "MIT"

  spec.files = Dir["{app,config,db,lib}/**/*", "LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "rails"
  spec.add_dependency "haml"
  spec.add_development_dependency "sqlite3"
end
