require "authenticated_app/engine"
require "random_code"
require "kramdown"

class UnauthenticatedRequest < StandardError; end
class UnauthorizedRequest < StandardError; end
class SuspendedRequest < StandardError; end
class ForbiddenRequest < StandardError; end

module AuthenticatedApp
  mattr_accessor :home_url   # home when authenticated
  mattr_accessor :root_url   # home when not authenticated
  mattr_accessor :email_only # if true, we ignore username and just use email
  mattr_accessor :from_address
end
